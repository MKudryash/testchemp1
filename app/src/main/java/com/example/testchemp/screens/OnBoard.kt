package com.example.testchemp.screens

import android.content.Intent
import android.os.Bundle
import android.os.MessageQueue
import android.view.View
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.testchemp.MainActivity
import com.example.testchemp.R
import com.example.testchemp.databinding.ActivityOnBoardBinding
import com.example.testchemp.databinding.ActivitySplashBinding
import com.example.testchemp.models.OnBoardModel
import io.paperdb.Paper
import java.util.Queue

class OnBoard : AppCompatActivity(), AnimationListener {
    private lateinit var binding: ActivityOnBoardBinding
    lateinit var queue: Queue<OnBoardModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOnBoardBinding.inflate(layoutInflater)
        setContentView(binding.root)
        pressBtns()
        exstractelement()
    }

    fun exstractelement() {
        Paper.book().write("queue", queue)
        parseElement(queue.poll()!!)
    }

    private fun parseElement(el: OnBoardModel) {
        with(binding) {
            txtNextTittle.text = el.tittle
            txtStartTittle.text = el.tittle
            txtDescription.text = el.description
            btn.text = el.button
            img.setImageResource(el.img)
            parseState(el.state)
        }
    }

    private fun parseState(state: Int) {
        with(binding) {
            when(state) {
                1 -> {
                    lines1.visibility = View.VISIBLE
                    lines2.visibility = View.GONE
                    lines3.visibility = View.GONE
                }
                2 -> {
                    txtStartTittle.visibility = View.GONE
                    txtNextTittle.visibility = View.VISIBLE
                    txtDescription.visibility = View.VISIBLE
                    lines1.visibility = View.GONE
                    lines2.visibility = View.VISIBLE
                    lines3.visibility = View.GONE
                }
                3 -> {
                    lines1.visibility = View.GONE
                    lines2.visibility = View.GONE
                    lines3.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun pressBtns() {
        with(binding){
            btn.setOnClickListener {
                if(queue.size == 1) {
                    startActivity(Intent(this@OnBoard, MainActivity::class.java))
                }
            }
        }
    }

    override fun onAnimationStart(animation: Animation?) {

    }

    override fun onAnimationEnd(animation: Animation?) {
        TODO("Not yet implemented")
    }

    override fun onAnimationRepeat(animation: Animation?) {

    }
}