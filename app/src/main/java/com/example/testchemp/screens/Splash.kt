package com.example.testchemp.screens

import android.content.Intent
import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.matuleme.objects.PrefManager
import com.example.testchemp.R
import com.example.testchemp.databinding.ActivitySplashBinding
import java.util.concurrent.TimeUnit

class Splash : AppCompatActivity() {
    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val thread: Thread = object : Thread() {
            override fun run() {
                try {
                    TimeUnit.SECONDS.sleep(2)
                    if(PrefManager.act == 0) {
                        startActivity(Intent(this@Splash, OnBoard::class.java))
                        finish()
                    }
                    /*if(PrefManager.act == 1) {
                        startActivity(Intent(this@Splash, SignIn::class.java))
                        finish()
                    }
                    if(PrefManager.act == 2) {
                        startActivity(Intent(this@Splash, Home::class.java))
                        finish()
                    }*/
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
        }
        thread.start()
    }

}