package com.example.testchemp.models

data class OnBoardModel(
    var img: Int,
    var tittle: String,
    var description: String,
    var button: String,
    var state: Int
)
